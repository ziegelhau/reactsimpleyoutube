import _ from 'lodash';

import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import YTSearch from 'youtube-api-search';
// SearchBar is the name of the variable. export default searchBar is what function to export
// Don't need to include the file extension if it is a .js file
import SearchBar from "./components/search_bar"; 
import VideoList from "./components/video_list";
import VideoDetail from "./components/video_detail";
const API_KEY = "AIzaSyCHSf0y3H7iIHgAaZHJFUXZsPZU-dBpmEc";

// Parent should be responsible for data that flows to child in react. 
// App is parent, so app should be responsible for fecting data

// Create new component. Should produce some html
// JSX, subset of javascript, which allows us to write basically HTML inside our javascript
// Will be compiled into vanilla javascript
// makes things a lot more legible

// This is the App class and not an instance of it
// can do function() or () => . Actually makes the value of "this" slightly different
class App extends Component {    
    constructor(props) {
        super(props);

        // initialize this.state.videos to an empty array
        this.state = {
            videos: [],
            selectedVideo: null
        };             
        this.videoSearch('surfboards');
    }

    videoSearch(term){
        YTSearch({key: API_KEY, term: term}, (videos) => {
            this.setState({
                videos: videos,
                selectedVideo: videos[0]
            }); 
        });
    }

    // below, we are passing this.state.videos as var = videos to VideoList.
    // So VideoList can now acess props.videos

    // onVideoSelect is a function somehow
    // onSearchTermChange is a function that takes a new search, calls this.videoSearch,  and passes the term to it
    render() {

        // debounced version of the function?
        // debounce takes the inner function, that can only be called once every 300 milliseconds
        const videoSearch = _.debounce((term) => {this.videoSearch(term)}, 300);

        return (
            <div>
                <SearchBar onSearchTermChange={videoSearch} />
                <VideoDetail video={this.state.selectedVideo}/>
                <VideoList                 
                onVideoSelect = {selectedVideo => this.setState({selectedVideo})}
                videos = {this.state.videos} />
            </div>
            );
    }
}

// Take this components generated html and put it on the page
// (in the DOM)
// <App/> creates an instance of app. Also shorthand for <App></App> (I think that's correct)
// Current error: is not a DOM Element, to fix do this
// add second argument to say put in this existing html document
ReactDOM.render(<App/>, document.querySelector(".container")); 