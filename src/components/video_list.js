import React from 'react';
import VideoListItem from './video_list_item';

// Use iterators instead of for loops in javascript
const VideoList = (props) => {
    // maps is a loop with a function inside
    const videoItems = props.videos.map((video) =>  {
        return (
        <VideoListItem 
        onVideoSelect={props.onVideoSelect}
        key={video.etag}
        video={video} />
        );
    });
    
    return (
        <ul className="col-md-4 list-group">                    
        {videoItems}
        </ul>
    );
}

export default VideoList;