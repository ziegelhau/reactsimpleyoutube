// Needed to write html inside your javascript
import React, { Component} from 'react';

// Need ability to be aware that it is being typed into it
// Need to make a class based component. Using es6 class (es2016)
// Should start with a function, then use a class if you need additional functionality
class SearchBar extends Component { // Don't have to do extends React.Component because of how we imported

    // only classes have states, functions do not
    constructor(props) {
        // Component has it's own constructor, we can call the parent method by calling super        
        super(props);

        // Initialize state by assigning. Record the property term (search term)
        // Initialize this.state.term to empty string
        // This state is only for the component SearchBar, not a global state
        this.state = { term: '' };
    }

    // Must have render method/function if we make a React class and return something
    render() {
        // says create a new input element
        // and pass it a prop (property) called on change with a value of this.onInputChange
        // onChange is a property

        // to change state always do this.setState        
        // Sets this.state.ter to event.target.value
        // good convention to have the class name be lowercase dash separated of the class name
        return (
            <div className="search-bar">
            <input 
            // makes input a controlled form
                value = {this.state.term}
                onChange= {event => this.onInputChange(event.target.value)} />
            </div>
        )
    };

    // sets the state of this component
    // then fires off the callback fuction onSearchTermChange
    onInputChange(term){
        this.setState({term})
        this.props.onSearchTermChange(term);        
    }
    // on or handle is a good convention, then name of element, then name of event itself
    // onInputChange(event) {
    //     console.log(event.target.value);
    // }
}

// Above is the same
// const SearchBar = () => {
//     // creates an input box
//     return <input /> // don't need to close the input because we're not going to close anything
// };

// node.js
// module.exports.SearchBar = SearchBar;
// React
export default SearchBar
